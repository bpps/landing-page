function bgBlink() {
  var menuItems = document.getElementsByClassName('TestBlocks')
  var blink = document.getElementById('GloBlink')
  for ( var i = 0; i < menuItems.length; i++ ) {
    menuItems[i].addEventListener('mouseenter', function () {
      blink.classList.add('active')
    })
    menuItems[i].addEventListener('mouseleave', function () {
      blink.classList.remove('active')
    })
  }
}
